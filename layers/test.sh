#!/bin/sh

if grep 'SUPER SECRET VALUE' history.txt ; then
	if tar xOf layer.tar | grep 'SUPER SECRET VALUE' ; then
		echo "OK"
	else
		echo "Missing from layer.tar"
		echo "Failed"
		exit 1
	fi
else
	echo "Missing from history.txt"
	echo "Failed"
	exit 2
fi
