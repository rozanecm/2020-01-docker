# docker-exercises

## file-server

Change ```start.sh``` to start an NGINX server, which serves ```files``` on a port it receives as an
argument.

### To test

```
./test.sh <PORT>
```

## layers

The provided Dockerfile writes "SUPER SECRET VALUE" to the image's filesystem, and then deletes
it.

Show that the value is still in several places in the image by:

- Writing the output of ```docker history``` to "history.txt"
- Using ```docker save```, and extracting the relevant layer to "layer.tar"

### To test

```
./test.sh
```

## without-compiler

Java has two common distributions:

- The JRE (Java Runtime Environment) is used to run Java programs
- The JDK (Java Development Kit) is used to compile Java programs

Change the Dockerfile so that Main.java will still compile and run, but it doesn't have a Java
compiler to access.

### To test

```
./test.sh
```

## compose-proxy

Configure a docker-compose with these three services:

1. Serves files_1
2. Serves files_2
3. Uses NGINX's [upstream module](http://nginx.org/en/docs/http/ngx_http_upstream_module.html) and
   proxy_pass to forward requests to services 1 and 2

Services 1 and 2 must not be accessible from the outside, service 3 must be accessible on localhost
on a port of your choice.

### To test

```
./test.sh <PORT>
```
